echo "Running the input preparation for the turbulent simulation"
#printing a message on the screen 
n=1
#initialize a dummy variable to 1

for Re in 50000 100000 500000 1000000
do
#loop on reynolds number
	m=1
	for zMesh in 512 1024
	do
#loop on zMesh
		o=1
		for yMesh in 128 256
		do
			g=1
			for xMesh in 256 512
			do
#loop on yMesh
			mkdir Anthony${n}_${m}_${o}_${g}
			cd originalF
#creating a folder named simulation where n m and o are replaced by their value.
#entering the folder originalF
			sed -e "s/rrrrrrr/${Re}/" -e "s/xxxx/${zMesh}/" -e "s/yyy/${yMesh}" -e "s/zzzzzzz/${xMesh}" inputOrig > input.dat
#replacing in inputOrig rrrrrrr with the value Re, xxxx with zMesh and yyy with yMesh and saving the results in input.dat
			cp input.dat ../Anthony${n}_${m}_${o}_${g}
#copying input.dat to the created folders (simulation*_*_*)
			cd ../
#returning to the previous folder
echo "Creating new folder Anthony_${n}_${m}_${o}_${g} with values of Re=${Re}, zMesh=${zMesh}, yMehs=${yMesh}, xMesh=${xMesh},in the input file  imput.dat"
				g = $(( $g + 1))
			done
			o=$(( $o + 1 ))
		done
#inncrementing o by 1
		m=$(( $m + 1 ))
	done
#incrementing m by 1 
	n=$(( $n + 1 ))
done
#incrementing n by 1
echo "input file preparation is done"
#printing a message.
